import { LitElement, html } from "lit-element";

class RequestSidebar extends LitElement{
    static get properties () {
        return {
            requestStats: {type: Object}
        };
    }


    constructor () {
        super ();
        this.requestStats = {};
    }

    render () {
        return html `
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <aside>
            <section>
                <div>
                    Hay <span class="badge bg-pill bg-primary">${this.requestStats.numberOfRequst}</span> peticiones
                </div>              
                <div class="mt-5">
                    <button @click="${this.newRequest}" class="w-100 btn bg-success" style="font-size: 20px"><strong>Abir Petición</strong></button>
                    <br>
                    <button @click="${this.goBack}" class="w-100 btn bg-danger" style="font-size: 20px"><strong>Volver a Usuarios</strong></button>
                </div>
            </section>
        </aside>
        `;
    }

    newRequest (e) {
        console.log ("newRequest en request-sidebar");
        console.log ("Se va a crear una nueva petición");

        this.dispatchEvent (new CustomEvent ("new-request", {}));
    }


    goBack (e) {
        console.log ("goBack en request-sidebar");
        console.log ("Se va a volver al menu de usuarios");

        this.dispatchEvent (new CustomEvent ("go-back", {}));
    }


}

customElements.define ("request-sidebar", RequestSidebar);