import { LitElement, html } from "lit-element";

class PersonaSidebar extends LitElement{
    static get properties () {
        return {
            peopleStats: {type: Object}
        };
    }


    constructor () {
        super ();
        this.peopleStats = {};
    }

    render () {
        return html `
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <aside>
            <section>
                <div>
                    Hay <span class="badge bg-pill bg-primary">${this.peopleStats.numberOfPeople}</span> usuarios
                </div>              
                <div class="mt-5">
                    <button @click="${this.newPerson}" class="w-100 btn bg-success" style="font-size: 20px"><strong>Agregar Usuario</strong></button>
                </div>
            </section>
        </aside>
        `;
    }

    newPerson (e) {
        console.log ("newPerson en persona-sidebar");
        console.log ("Se va a crear una nueva persona");

        this.dispatchEvent (new CustomEvent ("new-person", {}));
    }


}

customElements.define ("persona-sidebar", PersonaSidebar);