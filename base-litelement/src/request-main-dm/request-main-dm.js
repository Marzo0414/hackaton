import { LitElement, html } from "lit-element";

class RequestMainDM extends LitElement{
    static get properties () {
        return {
            request: {type: Array},
            showRequestList: {type: Boolean}                    
        };
    }


    constructor () {        
        console.log ("Ha entrado en el constructor de requestmain-dm.js");
        super ();
        this.request = [
            {
                requestId: "CRQ00001",
                description: "Primer CRQ",
                startDate: "10/06/2021",
                requestUserId: "e023956",
                authoriUserId: "1",
                resolutUserId: "2",
                resolutionDate: "3",
                authorisatDate: "4"
            },
            {
                requestId: "CRQ00002",
                description: "Primer CRQ",
                startDate: "10/06/2021",
                requestUserId: "e023956",
                authoriUserId: "1",
                resolutUserId: "2",
                resolutionDate: "3",
                authorisatDate: "4"
            },
            {
                requestId: "CRQ00003",
                description: "Primer CRQ",
                startDate: "10/06/2021",
                requestUserId: "e023956",
                authoriUserId: "1",
                resolutUserId: "2",
                resolutionDate: "3",
                authorisatDate: "4"
            },
            {
                requestId: "CRQ00004",
                description: "Primer CRQ",
                startDate: "10/06/2021",
                requestUserId: "e023956",
                authoriUserId: "1",
                resolutUserId: "2",
                resolutionDate: "3",
                authorisatDate: "4"
            },
            {
                requestId: "CRQ00005",
                description: "Quinto CRQ",
                startDate: "10/06/2021",
                requestUserId: "e023956",
                authoriUserId: "1",
                resolutUserId: "2",
                resolutionDate: "3",
                authorisatDate: "4"
            },
            {
                requestId: "CRQ00006",
                description: "Sexto CRQ",
                startDate: "10/06/2021",
                requestUserId: "e023956",
                authoriUserId: "1",
                resolutUserId: "2",
                resolutionDate: "3",
                authorisatDate: "4"
            },
            {
                requestId: "CRQ00007",
                description: "Septimo CRQ",
                startDate: "10/06/2021",
                requestUserId: "e023956",
                authoriUserId: "1",
                resolutUserId: "2",
                resolutionDate: "3",
                authorisatDate: "4"
            }
        ];
        this.showRequestList = false;
    }

        updated(changedProperties) {
            console.log ("updated en request-main-dm");

            if (changedProperties.has("request")) {
                console.log ("Ha cambiado el valor de la propiedad request en request-main-dm " + this.request);

                this.dispatchEvent(new CustomEvent ("request-data-updated",
                {
                    detail: {
                        request: this.request
                    }
                }
                ))
            }

            if (changedProperties.has("showRequestList")) {
                console.log ("Ha cambiado el valor de la propiedad showRequestList en request-main-dm " + this.showRequestList);

            }
        }

}

customElements.define ("request-main-dm", RequestMainDM);