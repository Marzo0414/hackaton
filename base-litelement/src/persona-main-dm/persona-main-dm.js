import { LitElement, html } from "lit-element";

class PersonaMainDM extends LitElement{
    static get properties () {
        return {
            people: {type: Array},
            showPersonForm: {type: Boolean},
                    
        };
    }


    constructor () {        
        super ();
        this.getUsers ();
        /*
        this.people = [
            {
                photo: {
                    src: "./img/persona1.jpg",
                    alt: "No hay foto"
                },
                userId: "e23956",     //Codigo de Usuario
                name: "Mario",        //Nombre,
                firstname: "Hernando",
                surname:   "Redondo",
                profile: "Peticionario"          //Nivel de autorización
            },
            {
                photo: {
                    src: "./img/persona2.jpg",
                    alt: "No hay foto"
                },
                userId: "e23966",     //Codigo de Usuario
                name: "Juan Luis",        //Nombre,
                firstname: "Hernando",
                surname:   "Redondo",
                profile: "Peticionario"          //Nivel de autorización
            },
            {
                photo: {
                    src: "./img/persona3.jpg",
                    alt: "No hay foto"
                },
                userId: "e23444",     //Codigo de Usuario
                name: "Luis",        //Nombre,
                firstname: "Hernando",
                surname:   "Redondo",                
                profile: "Autorizador"        //Nivel de autorización
            },
            {
                photo: {
                    src: "./img/persona4.jpg",
                    alt: "No hay foto"
                },
                userId: "e23999",     //Codigo de Usuario
                name: "Pepe",        //Nombre,
                firstname: "Hernando",
                surname:   "Redondo",
                profile: "Resolutor"        //Nivel de autorización
            }          
        ];
        */
        
    }

        updated(changedProperties) {
            console.log ("updated en persona-main-dm");

            if (changedProperties.has("people")) {
                console.log ("Ha cambiado el valor de la propiedad people en persona-main-dm");

                this.dispatchEvent(new CustomEvent ("people-data-updated",
                {
                    detail: {
                        people: this.people
                    }
                }
                ))
            }
        }

        getUsers () {            
            console.log ("getUsers");
            console.log ("Obteniendo los datos de los usuarios");
            /*
            let xhr = new XMLHttpRequest ();
                xhr.onload  = () => {
                    if (xhr.status === 200) {
                        console.log ("Petición completada correctamente");
                        let APIResponse = JSON.parse (xhr.responseText);
                        //console.log (APIResponse);
                        this.people = APIResponse.results;
                    }

                    if (xhr.status === 503) {
                        console.log ("Error 503");
                    }

                };
    
                //xhr.open ("GET", "http://localhost:8081/apicambios/usuarios");
                //xhr.send ();
                */


            //if (this.people.length () === 0) {
                this.people = [
                {
                    photo: {
                        src: "./img/persona1.jpg",
                        alt: "No hay foto"
                    },
                    userId: "e23956",     //Codigo de Usuario
                    name: "Mario",        //Nombre,
                    firstname: "Hernando",
                    surname:   "Redondo",
                    profile: "Peticionario"          //Nivel de autorización
                },
                {
                    photo: {
                        src: "./img/persona2.jpg",
                        alt: "No hay foto"
                    },
                    userId: "e23966",     //Codigo de Usuario
                    name: "Juan Luis",        //Nombre,
                    firstname: "Hernando",
                    surname:   "Redondo",
                    profile: "Peticionario"          //Nivel de autorización
                },
                {
                    photo: {
                        src: "./img/persona3.jpg",
                        alt: "No hay foto"
                    },
                    userId: "e23444",     //Codigo de Usuario
                    name: "Luis",        //Nombre,
                    firstname: "Hernando",
                    surname:   "Redondo",                
                    profile: "Autorizador"        //Nivel de autorización
                },
                {
                    photo: {
                        src: "./img/persona4.jpg",
                        alt: "No hay foto"
                    },
                    userId: "e23999",     //Codigo de Usuario
                    name: "Pepe",        //Nombre,
                    firstname: "Hernando",
                    surname:   "Redondo",
                    profile: "Resolutor"        //Nivel de autorización
                }          
            ];
            //}
            console.log ("Fin del getUsers")

        }
}

customElements.define ("persona-main-dm", PersonaMainDM);