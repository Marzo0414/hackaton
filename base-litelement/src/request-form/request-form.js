import { LitElement, html } from "lit-element";

class RequestForm extends LitElement{
    static get properties () {
        return {
            request: {type: Object},
            hoy:     {type: String}
        };
    }

    constructor () {
        super ();
        this.request = [];
        this.fechaHoy ();
        console.log ("hoy es " + this.hoy)
        this.resetFormData ();
    }

    render () {
        return html `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
            <div>
                <form>
                    <div class="form-group">
                        <label>Codigo de usuario peticionario</label>
                        <input @input="${this.updateRequestUserId}" 
                        .value="${this.request.requestUserId}" 
                        type="text" class="form-control" placeholder="Codigo de usuario peticionario" />
                    </div>
                    <div class="form-group">
                        <label>Id de la peticion</label>
                        <input @input="${this.updateRequestId}" 
                        .value="${this.request.requestId}" 
                        type="text" class="form-control" placeholder="Id de la peticion" />
                    </div>
                    <div class="form-group">
                        <label>Descripcion</label>
                        <input @input="${this.updateDescription}" 
                        .value="${this.request.description}" 
                        type="text" class="form-control" placeholder="Descripcion" />
                    </div>

                    <button @click="${this.goBack}" class="btn btn-primary"><strong>Atras</strong></button>
                    <button @click="${this.storeRequest}" class="btn btn-success"><strong>Guardar</strong></button>
                </form>
            </div>
        `;
    }


    goBack (e) {
        console.log("goBack");
        console.log("Se ha cerrado el formulario de Petición");
        e.preventDefault ();

        this.resetFormData ();
        this.dispatchEvent (new CustomEvent("request-form-close", {}));
    }

    storeRequest (e) {
        console.log("storeRequest");
        console.log("Se va a almacenar una petición");
        e.preventDefault ();
        console.log ("La propiedad requestId de request vale " + this.request.requestId);
        console.log ("el e.target.value vale: " + e.target.value)
        this.dispatchEvent (new CustomEvent("request-form-store",{
            detail: {
                request: {
                    requestId: this.request.requestId,
                    description: this.request.description,
                    requestUserId: this.request.requestUserId,
                    startDate : this.hoy,
                    authoriUserId : "",
                    resolutUserId : "",
                    resolutionDate : "",
                   authorisatDate : ""
                },
            }
        })
        );
    }

    fechaHoy () {
        let tiempoTranscurrido = Date.now();
        let hoy = new Date(tiempoTranscurrido);
        this.hoy= hoy.toLocaleDateString();
    }

    updateRequestId (e) {
        console.log ("updateRequestId");
        console.log ("Actualizando el valor de la propiedad requestId de person con valor " + e.target.value);
        this.request.requestId = e.target.value;
    }

    updateRequestUserId (e) {
        console.log ("updateRequestUserId");
        console.log ("Actualizando el valor de la propiedad requestUserId de person con valor " + e.target.value);
        this.request.requestUserId = e.target.value;
    }

    updateDescription (e) {
        console.log ("updateDescription");
        console.log ("Actualizando el valor de la propiedad description de person con valor " + e.target.value);
        this.request.description = e.target.value;
    }

    resetFormData() {
        console.log ("resetFormData");
        this.request = [];
        this.request.requestId = "";
        this.request.description = "";
        this.request.startDate = "";
        this.request.requestUserId = "";
        this.request.authoriUserId = "";
        this.request.resolutUserId = "";
        this.request.resolutionDate = "";
        this.request.authorisatDate = "";
    }
}

customElements.define ("request-form", RequestForm);