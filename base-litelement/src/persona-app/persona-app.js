import { LitElement, html } from "lit-element";
import '../app-header/app-header.js';
import '../persona-main/persona-main.js';
import '../app-footer/app-footer.js';
import '../persona-sidebar/persona-sidebar.js';
import '../request-sidebar/request-sidebar.js';
import '../persona-stats/persona-stats.js';

class PersonaApp extends LitElement{
    static get properties () {
        return {
                people: {type: Array},
                showPerson: {type: Boolean},
                showRequest: {type: Boolean},
        };
    }


    constructor () {
        super ();
        this.showPerson =  true;
        this.showRequest = false;
}

    render () {
        return html `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
            <app-header></app-header>
            <div class="row">
                <persona-sidebar 
                    @new-person="${this.newPerson}"                    
                    class="col-2">
                </persona-sidebar>
                <request-sidebar                    
                    class="col-2 d-none"
                    @new-request="${this.newRequest}">
                    @go-back="${this.goUsuarios}"                  
                </request-sidebar>
                <persona-main    
                    @updatedpeople="${this.updatePeople}"
                    @see-requests-sidebar="${this.seeRequestSidebar}"
                    class="col-10"></persona-main>
            </div>
            <app-footer></app-footer>
            <persona-stats  @updated-people-stats= "${this.updatePeopleStats}"></persona-stats>

        `;
    }

    updated (changedProperties) {
        console.log("updated en persona-app");
        console.log(changedProperties);

        if (changedProperties.has ("people")) {
            console.log ("Ha cambiado la propiedad people en persona-app");
            this.shadowRoot.querySelector ("persona-stats").people = this.people;
        }
    }


    newPerson (e) {
        console.log ("newPerson en persona-app");
        this.shadowRoot.querySelector ("persona-main").showPersonForm = true;        
    }

    newRequest (e) {
        console.log ("newRequest en persona-app");
        this.shadowRoot.querySelector ("persona-main").showRequestForm = true;
    }


    goUsuarios (e) {
        console.log ("goUsuarios en persona-app");
        this.shadowRoot.querySelector ("persona-main").showRequestList = false;
    }


    seeRequestSidebar (e) {
        console.log ("seeRequestSidebar en persona-app");
        this.shadowRoot.querySelector ("persona-sidebar").classList.add("d-none");
        this.shadowRoot.querySelector ("request-sidebar").classList.remove("d-none");
        this.shadowRoot.querySelector ("app-header").title = "Peticiones";
    }


    updatePeople (e) {
        console.log ("updatePeople en persona-app");        
        this.people = e.detail.people;        
    }


    updatePeopleStats (e) {
        console.log ("updatePeople en persona-app");
        console.log (e.detail);

        this.shadowRoot.querySelector("persona-sidebar").peopleStats = e.detail.peopleStats;        
    }
}

customElements.define ("persona-app", PersonaApp);