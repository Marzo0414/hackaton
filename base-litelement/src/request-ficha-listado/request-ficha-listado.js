import { LitElement, html } from "lit-element";

class RequestFichaListado extends LitElement{
    static get properties () {
        return {
            requestId: {type: String},
            description: {type: String},
            startDate: {type: String},
            requestUserId: {type: String},
            authoriUserId: {type: String},
            resolutUserId: {type: String},
            resolutionDate: {type: String},
            authorisatDate: {type: String}
        }
    }


    constructor () {
        super ();
    }

    
    render () {
        return html `

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
            <div class="card h-100">
                <div class="card-body">
                    <h5 class="card-title">${this.requestId}</h5>
                    <h5 class="card-title">${this.description}</h5>
                    <h5 class="card-title">${this.startDate}</h5>
                    <h5 class="card-title">${this.requestUserId}</h5>
                    <h5 class="card-title">${this.authoriUserId}</h5>
                    <h5 class="card-title">${this.resolutUserId}</h5>
                    <h5 class="card-title">${this.resolutionDate}</h5>
                    <h5 class="card-title">${this.authorisatDate}</h5>
                </div>
            </div>
        `;
    }

    goBack () {

    }

    updated ()
    {
        console.log ("updated en request-ficha-listado");
    }
    

}

customElements.define ("request-ficha-listado", RequestFichaListado);