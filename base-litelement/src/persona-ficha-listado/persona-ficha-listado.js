import { LitElement, html } from "lit-element";

class PersonaFichaListado extends LitElement{
    static get properties () {
        return {
            fname: {type: String},
            firstname: {type: String},
            surname: {type: String},
            userId: {type: String},
            photo: {type: Object},
            profile: {type: String}
        };
    }


    constructor () {
        super ();
}

    render () {
        return html `
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
            <div class="card h-100">
            <img src="${this.photo.src}"  alt="${this.photo.alt}"
                height ="300" width="250" class="card-img-top">
            <div class="card-body">
            <h5 class="card-title">${this.userId}</h5>
            <p class="card-text">${this.fname} ${this.firstname} ${this.surname}</p>
            <p class="card-text">${this.profile}</p>
            </div>
            <div class="card-footer">
                <button @click="${this.deletePerson}" class="btn btn-danger col-5"><strong>Borrar</strong></button>
                <button @click="${this.editPerson}" class="btn btn-info offset-1 col-5"><strong>Editar</strong></button>
                <br>
                <br>
                <button @click="${this.seeRequests}" class="btn btn-success offset-2 col-7"><strong>Peticiones</strong></button>
            </div>
        `;
    }

    deletePerson (e) {
        console.log ("deletePerson en persona-ficha-listado");
        console.log ("Se va a borrar la persona de nombre: " + this.fname);

        this.dispatchEvent(
            new CustomEvent ("delete-person", {
                detail: {
                    name: this.fname
                }
            })
        )
    }

    editPerson (e) {
        console.log ("editPerson en persona-ficha-listado");
        console.log ("Se va a editar la persona " + this.fname);

        this.dispatchEvent (
            new CustomEvent ("edit-person", {
                detail: {
                    name: this.fname
                }
            }
            )
        )

    }


    seeRequests (e) {
        console.log ("seeRequests en persona-ficha-listado");
        this.dispatchEvent (
            new CustomEvent ("see-requests", {
                detail: {
                    name: this.fname
                }
            }
            )
        )
    }
}

customElements.define ("persona-ficha-listado", PersonaFichaListado);