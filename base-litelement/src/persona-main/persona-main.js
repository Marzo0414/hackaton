import { LitElement, html } from "lit-element";
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../request-ficha-listado/request-ficha-listado.js';
import '../persona-main-dm/persona-main-dm.js';
import '../request-main-dm/request-main-dm.js';
import '../persona-form/persona-form.js';
import '../request-form/request-form.js';


class PersonaMain extends LitElement{
    static get properties () {
        return {
            people: {type: Array},
            request: {type: Array},
            showPersonForm: {type: Boolean},
            showRequestForm: {type: Boolean},
            showRequestList: {type: Boolean}
        };
    }

    constructor () {
        super ();
        this.showPersonForm = false;
        this.showRequestForm = false;
        this.showRequestList = false;
        this.people = [];
        this.request = [];
    }

    render () {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">            
            <div class="row" id="peopleList">
                <div 
                    class="row row-cols-1 row-cols-sm-4">        
                        ${this.people.map (
                            person => html`<persona-ficha-listado
                                profile="${person.profile}"
                                fname="${person.name}"
                                firstname="${person.firstname}"
                                surname="${person.surname}"
                                userId="${person.userId}"
                                .photo="${person.photo}"
                                @delete-person="${this.deletePerson}"
                                @edit-person="${this.editPerson}"
                                @see-requests="${this.seeRequests}"
                                ></persona-ficha-listado>                                
                                `
                    )}                
                
                </div>
            </div>
            <div class="row" id="requestList">
                <div 
                    class="row row-cols-1 row-cols-sm-4">        
                        ${this.request.map (
                            request => html`<request-ficha-listado
                                requestId="${request.requestId}"
                                description="${request.description}"
                                startDate="${request.startDate}"
                                requestUserId="${request.requestUserId}"
                                authoriUserId="${request.authoriUserId}"
                                resolutUserId="${request.resolutUserId}"
                                resolutionDate="${request.resolutionDate}"
                                authorisatDate="${request.authorisatDate}"

                                >
                                </request-ficha-listado>                                
                                `
                    )}                
                
                </div>
            </div>
            <div class="row">
                        <request-ficha-listado 
                            class="d-none border rounded border-primary" 
                            id="requestList">
                        </request-ficha-listado>
            </div>

            <div class="row">
                        <persona-form 
                            @persona-form-close="${this.personFormClose}" 
                            @persona-form-store="${this.personFormStore}" 
                            class="d-none border rounded border-primary" 
                            id="personForm">
                        </persona-form>
            </div>
            <div class="row">
                        <request-form
                            @request-form-close="${this.requestFormClose}" 
                            @request-form-store="${this.requestFormStore}"
                            class="d-none border rounded border-primary" 
                            id="requestForm">
                        </request-form>
            </div>

            <persona-main-dm @people-data-updated="${this.peopleDataUpdated}"></persona-main-dm>
            <request-main-dm @request-data-updated="${this.requestDataUpdated}"></request-main-dm>
        `;
    }


    updated (changedProperties) {
        console.log ("updated");

        if (changedProperties.has ("showPersonForm")) {
            console.log ("Ha cambiado el valor de la propiedad showPersonForm en persona-main");

            if (this.showPersonForm === true) {
                this.showPersonFormData ();
            } else {
                this.showPersonList ();
            }

        }

        if (changedProperties.has("people")) {
            console.log ("Ha cambiado el valor de la propiedad people en persona-main");
        }

        if (changedProperties.has("yearsRange")) {
            console.log ("Ha cambiado el valor de la propiedad yearsRange en persona-main");
        }
    }

    peopleDataUpdated (e) {
        console.log ("peopleDataUpdated en persona-main");
        this.people = e.detail.people;

    }

    requestDataUpdated (e) {
        console.log ("requestDataUpdated en persona-main");
        this.request = e.detail.request;
    }

    personFormClose () {
        console.log ("personFormClose en persona-main");
        console.log ("Se ha cerrado el formulario de persona");
        this.showPersonForm = false;
    }


    requestFormClose () {
        console.log ("requestFormClose en persona-main");
        console.log ("Se ha cerrado el formulario de request");
        this.showRequestForm = false;
        this.showRequestList = true;
    }

    requestListClose () {
        console.log ("requestListClose en persona-main");
        console.log ("Se ha cerrado el listado de request");
        this.showRequestList = false;
    }

    personFormStore (e) {
        console.log ("personFormStore");
        console.log ("Se va almacenar una persona");

        console.log ("La propiedad name de person vale " + e.detail.person.name);
        console.log ("La propiedad profile de person vale " + e.detail.person.profile);
 
        if (e.detail.editingPerson === true) {
            console.log ("Se va actualizar la persona de userId " + e.detail.person.userId);
            this.people = this.people.map(
                person => person.userId ===e.detail.person.userId
                ? person = e.detail.person : person
            );
        } else {
            console.log ("Se va almacenar una persona nueva");
            this.people = [...this.people, e.detail.person]

        }
        console.log ("Proceso terminado");
        this.showPersonForm = false;
    }

    requestFormStore (e) {
        console.log ("requestFormStore");
        console.log ("Se va almacenar una petición nueva");

        console.log ("La propiedad requestId de request vale " + e.detail.request.requestId);
        this.request = [...this.request, e.detail.request]

        console.log ("Proceso terminado");
        this.showRequestForm = false;
        this.showRequestList = true;
    }


    showPersonFormData() {
        console.log ("showPersonFormData");
        console.log ("Mostrando el formulario de persona");
        this.shadowRoot.getElementById ("peopleList").classList.add("d-none");
        this.shadowRoot.getElementById ("personForm").classList.remove("d-none");
        this.shadowRoot.getElementById ("requestList").classList.add("d-none");
        this.shadowRoot.getElementById ("requestForm").classList.add("d-none");
    }

    showPersonList () {
        console.log ("showPersonList");
        console.log ("Mostrando el listado de personas");
        this.shadowRoot.getElementById ("personForm").classList.add("d-none");
        this.shadowRoot.getElementById ("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById ("requestList").classList.add("d-none");
        this.shadowRoot.getElementById ("requestForm").classList.add("d-none");
    }

    showRequestsList () {
        console.log ("showRequestsList");
        console.log ("Mostrando el listado de Request");
        console.log ("La request vale: " + this.request);
        this.shadowRoot.getElementById ("personForm").classList.add("d-none");
        this.shadowRoot.getElementById ("peopleList").classList.add("d-none");
        this.shadowRoot.getElementById ("requestList").classList.remove("d-none");
        this.shadowRoot.getElementById ("requestForm").classList.add("d-none");
    }


    showRequestsForm () {
        console.log ("showRequestsForm");
        console.log ("Mostrando el formulario de Request");
        console.log ("La request vale: " + this.request);
        this.shadowRoot.getElementById ("personForm").classList.add("d-none");
        this.shadowRoot.getElementById ("peopleList").classList.add("d-none");
        this.shadowRoot.getElementById ("requestList").classList.add("d-none");
        this.shadowRoot.getElementById ("requestForm").classList.remove("d-none");
    }


    deletePerson (e) {
            console.log ("deletePerson en persona-main");
            console.log ("Se va a borrar la persona de nombre " + e.detail.name);

            this.people = this.people.filter (
                person => person.name != e.detail.name
            );
        }


    editPerson (e) {
        console.log ("editPerson en persona-main");
        console.log ("Se va a editar la información de la persona " + e.detail.name);

        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );

        let person = {};
        person.name = chosenPerson [0].name;
        person.profile = chosenPerson [0].profile;
        person.surname = chosenPerson [0].surname;
        person.firstname = chosenPerson [0].firstname;
        person.userId = chosenPerson [0].userId;

        this.shadowRoot.getElementById ("personForm").person = person;
        this.shadowRoot.getElementById ("personForm").editingPerson = true;
        this.showPersonForm =  true;
    }


    seeRequests (e) {
        console.log ("seeRequests en persona-main");

        let chosenPerson = e.detail.name;
        
        this.shadowRoot.querySelector ("request-main-dm").showRequestList = true;
        this.showRequestList =  true;
        this.showRequestsList ();
        this.dispatchEvent(new CustomEvent("see-requests-sidebar", {}));

    }


    updated (changedProperties) {
        console.log ("updated");


        if (changedProperties.has ("showRequestForm")) {
            console.log ("Ha cambiado el valor de la propiedad showRequestForm en persona-main " + this.showRequestForm);
            this.shadowRoot.getElementById ("requestForm").showRequestForm = true;
            if (this.showRequestForm === true) {
                this.showRequestsForm ();
            } else {
                this.showRequestsList ();
            }
        }

        if (changedProperties.has ("showPersonForm")) {
            if (this.showPersonForm === true) {
                this.showPersonFormData ();
            } else {
                this.showPersonList ();
            }
        }

        if (changedProperties.has ("people")) {
            console.log ("Ha cambiado el valor de la propiedad people en persona-main ");

            this.dispatchEvent (new CustomEvent ("updatedpeople", {
                detail:  {
                    people: this.people
                }
            }));
        }
        
        if (changedProperties.has ("showRequestList")) {
            console.log ("Ha cambiado el valor de la propiedad showRequestList en persona-main " + this.showRequestList);
            this.shadowRoot.getElementById ("requestList").showRequestList = true;            
        }
    }
}

customElements.define ("persona-main", PersonaMain);