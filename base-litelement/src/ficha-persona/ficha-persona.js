import { LitElement } from "lit-element";

class FichaPersona extends LitElement{
    static get properties () {
        return {
            fname: {type: String},
            yearsInCompany: {type: Number}
        }
    }


    constructor () {
        super ();

        this.name = "Prueba Nombre";
        this.yearsInCompany = 1;
    }

    updated (changedProperties) {
        changedProperties.forEach((oldValue, propName) => {
            console.log ("Ha cambiado el valor de la propiedad " + propName + " anterior era " + oldValue);
        })
    }

    render () {
        return html `
            <div>
                <label>Nombre Completo</label>
                <input type="text" id="fname" name="fname" value="${this.fname}"></input>
                <br />
                <label>Años en la empresa</label>
                <input type="text" value="${this.yearsInCompany}"></input>
                <br />
            </div>
        `;
    }
}

customElements.define ("ficha-persona", FichaPersona);