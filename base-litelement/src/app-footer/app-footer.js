import { LitElement, html } from "lit-element";

class AppFooter extends LitElement{
    static get properties () {
        return {
        };
    }


    constructor () {
        super ();
}

    render () {
        return html `
            <h5>@Aplicacion de peticiones</h5>
        `;
    }
}

customElements.define ("app-footer", AppFooter);