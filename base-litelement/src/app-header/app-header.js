import { LitElement, html } from "lit-element";

class AppHeader extends LitElement{
    static get properties () {
        return {
            title: {type: String}
        };
    }


    constructor () {
        super ();
        this.title = "Usuarios";
}

    render () {
        
        return html `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
            <div class="d-flex justify-content-center">${this.title}</div>
        `;
    }
}

customElements.define ("app-header", AppHeader);