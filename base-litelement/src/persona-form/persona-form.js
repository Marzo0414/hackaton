import { LitElement, html } from "lit-element";

class PersonaForm extends LitElement{
    static get properties () {
        return {
            person: {type: Object},
            editingPerson: {type: Boolean}
        };
    }


    constructor () {
        super ();
        this.resetFormData ();
    }

    render () {
        return html `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
            <div>
                <form>
                    <div class="form-group">
                        <label>Id de usuario</label>
                        <input @input="${this.updateUserId}" 
                        .value="${this.person.userId}" 
                        ?disabled="${this.editingPerson}"
                        type="text" class="form-control" placeholder="Id de usuario" />
                    </div>
                    <div class="form-group">
                        <label>Nombre</label>
                        <input @input="${this.updateName}" 
                        .value="${this.person.name}"                         
                        type="text" class="form-control" placeholder="Nombre" />
                    </div>
                    <div class="form-group">
                        <label>Primer Apellido</label>
                        <input @input="${this.updateSurname}" 
                        .value="${this.person.surname}" 
                        type="text" class="form-control" placeholder="Primer apellido" />
                    </div>
                    <div class="form-group">
                        <label>Segundo Apellido</label>
                        <input @input="${this.updateFirstname}" 
                        .value="${this.person.firstname}" 
                        type="text" class="form-control" placeholder="Segundo Apellido" />
                    </div>
                    <div class="form-group">
                        <label>Nivel de autorizador</label>
                        <select name ="profile"
                        .value="${this.person.profile}" 
                        @input="${this.updateProfile}">
                            <option>Peticionario </option>
                            <option>Autorizador</option>
                            <option>Resolutor</option> 
                        </select>
                    </div>
                    <br>
                    <button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
                    <button @click="${this.storePerson}" class="btn btn-success"><strong>Guardar</strong></button>
                </form>
            </div>
        `;
    }

    updateUserId (e) {
        console.log ("updateUserId");
        console.log ("Actualizando el valor de la propiedad userId de person con valor " + e.target.value);
        this.person.userId = e.target.value;
    }


    updateName (e) {
        console.log ("updateName");
        console.log ("Actualizando el valor de la propiedad name de person con valor " + e.target.value);
        this.person.name = e.target.value;
    }

    updateSurname (e) {
        console.log ("updateSurname");
        console.log ("Actualizando el valor de la propiedad surname de person con valor " + e.target.value);
        this.person.surname = e.target.value;
    }

    updateFirstname (e) {
        console.log ("updateFirstname");
        console.log ("Actualizando el valor de la propiedad firstname de person con valor " + e.target.value);
        this.person.firstname = e.target.value;
    }

    updateProfile (e) {
        console.log ("updateProfile");
        console.log ("Actualizando el valor de la propiedad profile de person con valor " + e.target.value);
        this.person.profile = e.target.value;
    }

    updatePhoto (e) {
        console.log ("updatePhoto");
        console.log ("Actualizando el valor de la propiedad photo.src de person con valor " + e.target.value);
        this.person.photo = e.target.value;

    }

    goBack (e) {
        console.log("goBack");
        console.log("Se ha cerrado el formulario de persona");
        e.preventDefault ();

        this.resetFormData ();
        this.dispatchEvent (new CustomEvent("persona-form-close", {}));
    }

    storePerson (e) {
        console.log("storePerson");
        console.log("Se va a almacenar una persona");
        e.preventDefault ();

        console.log ("La propiedad name de person vale " + this.person.name);
        console.log ("La propiedad profile de person vale " + this.person.profile);
        
        this.person.photo = {
            src: "./img/persona6.jpg",
            alt: "Persona"
        };
    

        this.dispatchEvent (new CustomEvent("persona-form-store",{
            detail: {
                person: {
                    userId: this.person.userId,
                    name: this.person.name,
                    surname: this.person.surname,
                    firstname: this.person.firstname,
                    profile: this.person.profile,
                    photo: this.person.photo
                },
                editingPerson: this.editingPerson
            }            
        })
        );
    }

    resetFormData() {
        console.log ("resetFormData");

        this.person = [];
        this.person.name = "";
        this.person.profile = "";
        this.person.surname = "";
        this.person.firstname = "";
        this.person.userId = "";

        this.editingPerson = false;
    }

    /*
    update () {
        if (this.editingPerson === false) {
            this.resetFormData ();
        }
    }
    */

}

customElements.define ("persona-form", PersonaForm);