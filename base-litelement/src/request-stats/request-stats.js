import { LitElement, html } from "lit-element";

class RequestStats extends LitElement{
    static get properties () {
        return {
            request: {type: Array}
        };
    }

    constructor () {
        super ();
        this.request = [];
    }

    updated (changedProperties) {
        console.log ("updated en request-stats");
        console.log (changedProperties);

        if (changedProperties.has("request")) {
            console.log ("Ha cambiado el valor de la propiedad request en persona-stats")

            let requestStats = this.gatherPeopleArrayInfo (this.request);

            this.dispatchEvent (new CustomEvent ("updated-request-stats" , {
                detail: {
                    requestStats: requestStats
                }
            }));
        }
    }

    gatherRequestArrayInfo (request) {
        console.log ("gatherRequestArrayInfo");

        let requestStats = {};
        peopleStats.numberOfPeople = people.length;

        let maxYearsInCompany = 0;

        people.forEach(
            person => {
                if (person.yearsInCompany > maxYearsInCompany) {
                    console.log ("Cambiando el maximo");
                    maxYearsInCompany = person.yearsInCompany;
                }
            }
        );

        console.log ("maxYearsInCompany es: " + maxYearsInCompany);
        peopleStats.maxYearsInCompany = maxYearsInCompany;

        return peopleStats;
    }


}

customElements.define ("request-stats", RequestStats);